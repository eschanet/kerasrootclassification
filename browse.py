import sys
import logging

import numpy as np
import matplotlib.pyplot as plt

from KerasROOTClassification import *

logging.basicConfig()
logging.getLogger("KerasROOTClassification").setLevel(logging.INFO)

c = load_from_dir(sys.argv[1])

cs = []
cs.append(c)

if len(sys.argv) > 2:
    for project_name in sys.argv[2:]:
        cs.append(load_from_dir(project_name))
